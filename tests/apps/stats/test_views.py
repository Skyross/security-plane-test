# -*- coding: utf-8 -*-
import pytest
from django.test.client import Client
from django.urls import reverse


@pytest.mark.django_db
def test_stats__ok(
    client: Client,
    django_db_data_setup: None,
) -> None:
    response = client.get(reverse("stats"))
    response_data = response.json()
    assert response.status_code == 200  # nosec
    assert response_data["average_request_time"] < 0.1  # nosec
    assert response_data["request_count"] == 1  # nosec
    assert response_data["vm_count"] == 2  # nosec
