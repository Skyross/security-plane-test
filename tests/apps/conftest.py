# -*- coding: utf-8 -*-
import pytest

# noinspection PyProtectedMember
from pytest_django.plugin import _DatabaseBlocker

from security_plane_test.apps.firewalls.models import FirewallRule, VirtualMachine
from security_plane_test.apps.firewalls.utils import load_data_file


@pytest.fixture(scope="session")
def django_db_data_setup(
    django_db_setup: None, django_db_blocker: _DatabaseBlocker
) -> None:
    with django_db_blocker.unblock():
        load_data_file("data/input-0.json")
        assert VirtualMachine.objects.count() == 2  # nosec
        assert FirewallRule.objects.count() == 1  # nosec
