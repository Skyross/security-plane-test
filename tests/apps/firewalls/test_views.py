# -*- coding: utf-8 -*-
from typing import List

import pytest
from django.test.client import Client
from django.urls import reverse


def test_attack__field_missing(client: Client) -> None:
    response = client.get(reverse("attack"))
    assert response.status_code == 400  # nosec
    assert response.json() == {"vm_id": ["This field is required."]}  # nosec


def test_attack__field_badly_formatted(client: Client) -> None:
    response = client.get(reverse("attack"), {"vm_id": "xxxxxxx"})
    assert response.status_code == 400  # nosec
    assert response.json() == {  # nosec
        "vm_id": ["Ensure value have 'vm-xxxxxxx' format"]
    }


@pytest.mark.django_db
def test_attack__virtual_machine_not_found(client: Client) -> None:
    response = client.get(reverse("attack"), {"vm_id": "vm-xxxxxxx"})
    assert response.status_code == 404  # nosec
    assert response.json() == {"detail": "Not found."}  # nosec


@pytest.mark.django_db
@pytest.mark.parametrize(
    "target,attackers",
    (
        ("vm-c7bac01a07", []),
        ("vm-a211de", ["vm-c7bac01a07"]),
    ),
)
def test_attack__ok(
    client: Client,
    django_db_data_setup: None,
    target: str,
    attackers: List[str],
) -> None:
    response = client.get(reverse("attack"), {"vm_id": target})
    assert response.status_code == 200  # nosec
    assert sorted(response.json()) == sorted(attackers)  # nosec
