# -*- coding: utf-8 -*-
from django.apps import AppConfig
from django.core.management import call_command


class CoreConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "security_plane_test.core"

    def ready(self) -> None:
        super().ready()

        # NOTE: this call is a kind of a bad practice but we use it
        # just for this test exercise
        call_command("migrate")
