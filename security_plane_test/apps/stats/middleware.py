# -*- coding: utf-8 -*-
import time
from dataclasses import dataclass
from typing import Any, Callable

from django.http import HttpResponse

from .utils import calculate_moving_average


@dataclass
class Stats:
    average_request_time: float = 0.0
    request_count: int = 0


class StatsMiddleware:
    """Middleware to gather overall statistics of server runtime.
    This functionality may use shared cache instance to store stats
    to allow runserver process scaling.
    """

    def __init__(self, get_response: Callable[[Any], Any]) -> None:
        """One-time configuration and initialization."""
        self.get_response = get_response
        self.stats = Stats()

    def __call__(self, request: Any) -> HttpResponse:
        request.stats = self.stats
        request.request_start_time = request_start_time = time.time()

        response = self.get_response(request)
        execution_time = time.time() - request_start_time

        self.stats.request_count += 1
        self.stats.average_request_time = calculate_moving_average(
            previous_average=self.stats.average_request_time,
            current_value=execution_time,
            number_of_samples=self.stats.request_count,
        )

        return response
