# -*- coding: utf-8 -*-
import time
from typing import Dict, Union

from rest_framework.generics import RetrieveAPIView

from ..firewalls.models import VirtualMachine
from .serializers import StatsSerializer
from .utils import calculate_moving_average


class StatsAPIView(RetrieveAPIView):
    """Endpoint will return service statistics in a JSON format:
    number of virtual machines in the cloud environment, number of
    requests to all endpoints & average request processing time
    (in seconds with a millisecond granularity). Statistics
    should be from process startup.

    NOTE: this endpoint heavily depends on
    security_plane_test.apps.stats.middleware.StatsMiddleware."""

    serializer_class = StatsSerializer

    def get_object(self) -> Dict[str, Union[int, float]]:
        stats = self.request.stats

        return {
            "vm_count": VirtualMachine.objects.count(),
            "request_count": stats.request_count + 1,
            # we need to recalculate calculate_moving_average
            # additionally to include this endpoint call into resulting
            "average_request_time": calculate_moving_average(
                previous_average=stats.average_request_time,
                current_value=time.time() - self.request.request_start_time,
                number_of_samples=stats.request_count + 1,
            ),
        }
