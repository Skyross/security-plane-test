# -*- coding: utf-8 -*-
from rest_framework.serializers import FloatField, IntegerField, Serializer


class StatsSerializer(Serializer):
    vm_count = IntegerField()
    request_count = IntegerField()
    average_request_time = FloatField()
