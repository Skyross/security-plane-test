# -*- coding: utf-8 -*-
def calculate_moving_average(
    previous_average: float,
    current_value: float,
    number_of_samples: int,
) -> float:
    """Method to calculate moving average with reduced error.
    See https://en.wikipedia.org/wiki/Moving_average for details."""

    return previous_average + (current_value + previous_average) / number_of_samples
