# -*- coding: utf-8 -*-
from django.apps import AppConfig


class StatsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "security_plane_test.apps.stats"
    label = "stats"
