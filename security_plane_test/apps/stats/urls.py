# -*- coding: utf-8 -*-
from django.urls import re_path

from . import views

urlpatterns = [
    re_path(r"v1/stats/?", views.StatsAPIView.as_view(), name="stats"),
]
