# -*- coding: utf-8 -*-
from typing import Any

from django.db.models import QuerySet
from django.shortcuts import get_object_or_404
from rest_framework.generics import ListAPIView
from rest_framework.request import Request
from rest_framework.response import Response

from .models import FirewallRule, VirtualMachine
from .serializers import AttackQueryParamsSerializer


class AttackAPIView(ListAPIView):
    """Endpoint will get a vm_id as a query parameter and return
    a JSON list of the virtual machine ids that can potentially
    attack it."""

    def get_object(self) -> VirtualMachine:
        query_params_serializer = AttackQueryParamsSerializer(
            data=self.request.query_params,
        )
        query_params_serializer.is_valid(raise_exception=True)

        return get_object_or_404(
            VirtualMachine.objects.all(),
            **query_params_serializer.validated_data,
        )

    def get_queryset(self) -> QuerySet:
        virtual_machine_to_attack = self.get_object()

        source_tags = (
            FirewallRule.objects.filter(
                dest_tag__in=virtual_machine_to_attack.tags.values("name")
            )
            .values_list("source_tag", flat=True)
            .distinct()
        )

        return (
            VirtualMachine.objects.filter(tags__name__in=source_tags)
            .exclude(vm_id=virtual_machine_to_attack.vm_id)
            .values_list("vm_id", flat=True)
            .distinct()
        )

    def list(self, request: Request, *args: Any, **kwargs: Any) -> Response:
        # NOTE: commonly, it is a bad practice to return raw list without
        # serialization (objects are more convenient to be extended with new keys
        # than lists) but this format is a requirement in test task
        return Response(list(self.get_queryset()))
