# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import gettext_lazy as _


class VirtualMachine(models.Model):
    """This model represents Virtual Machine."""

    vm_id = models.CharField(
        _("Virtual machine ID"),
        primary_key=True,
        max_length=128,
        help_text=_("An identifier that uniquely identifies a virtual machines."),
    )
    name = models.CharField(
        _("Name"),
        max_length=128,
        help_text=_("A user-friendly display name."),
    )

    class Meta:
        verbose_name = _("Virtual machine")
        verbose_name_plural = _("Virtual machines")

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__} {self.vm_id}>"


class VirtualMachineTag(models.Model):
    """Model to track tags for virtual machines. It only needed to streamline
    queries using sqlite3 as database. Need to switch to postgresql
    and use array field in future."""

    vm = models.ForeignKey(
        VirtualMachine,
        related_name="tags",
        on_delete=models.CASCADE,
    )
    name = models.CharField(
        _("Name"),
        max_length=128,
        help_text=_("A user-friendly display name."),
    )

    class Meta:
        unique_together = (("vm", "name"),)
        verbose_name = _("Virtual machine tag")
        verbose_name_plural = _("Virtual machine tags")


class FirewallRule(models.Model):
    """By default, a virtual machine has no access from external sources.
    If an administrator wants to make a virtual machine accessible to other
    machines, it defines a firewall rule to allow traffic."""

    fw_id = models.CharField(
        _("Firewall rule ID"),
        primary_key=True,
        max_length=128,
        help_text=_("An identifier that uniquely identifies a firewall rule."),
    )
    source_tag = models.CharField(
        _("Source tag"),
        max_length=128,
        help_text=_("A string that represents the source tag of a traffic."),
    )
    dest_tag = models.CharField(
        _("Destination tag"),
        max_length=128,
        help_text=_("A string that represents the destination tag of a traffic."),
    )

    class Meta:
        verbose_name = _("Firewall rule")
        verbose_name_plural = _("Firewall rules")

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__} {self.fw_id}>"
