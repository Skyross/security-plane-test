# -*- coding: utf-8 -*-
import sys

from django.apps import AppConfig
from django.conf import settings
from structlog import get_logger

from .utils import load_data_file

logger = get_logger(__name__)


class FirewallsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "security_plane_test.apps.firewalls"
    label = "firewalls"

    def ready(self) -> None:
        super().ready()

        # NOTE: this call is a kind of a bad practice but we use it
        # just for this test exercise
        if "runserver" in sys.argv and settings.FIREWALLS_IMPORT_FILE is not None:
            logger.info("data_import.started")
            load_data_file(settings.FIREWALLS_IMPORT_FILE)
            logger.info("data_import.finished")
