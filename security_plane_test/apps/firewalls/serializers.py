# -*- coding: utf-8 -*-
from django.utils.deconstruct import deconstructible
from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import CharField, ListField, Serializer


@deconstructible
class VirtualMachineIDValidator:
    message = _("Ensure value have 'vm-xxxxxxx' format")
    code = "bad_format"

    def __call__(self, value: str) -> None:
        if not value.startswith("vm-"):
            raise ValidationError(self.message, code=self.code)


class VirtualMachineID(CharField):
    default_validators = [VirtualMachineIDValidator()]


class VirtualMachineSerializer(Serializer):
    vm_id = VirtualMachineID()
    name = CharField()
    tags = ListField(child=CharField())


@deconstructible
class FirewallRuleIDValidator:
    message = _("Ensure value have 'vm-xxxxxxx' format")
    code = "bad_format"

    def __call__(self, value: str) -> None:
        if not value.startswith("vm-"):
            raise ValidationError(self.message, code=self.code)


class FirewallRuleID(CharField):
    default_validators = [FirewallRuleIDValidator]


class FirewallRuleSerializer(Serializer):
    fw_id = FirewallRuleID()
    source_tag = CharField()
    dest_tag = CharField()


class DataImportSerializer(Serializer):
    vms = VirtualMachineSerializer(many=True)
    fw_rules = FirewallRuleSerializer(many=True)


class AttackQueryParamsSerializer(Serializer):
    vm_id = VirtualMachineID()
