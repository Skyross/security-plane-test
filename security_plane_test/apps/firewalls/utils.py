# -*- coding: utf-8 -*-
import json
from pathlib import Path

from django.db import transaction
from structlog import get_logger

logger = get_logger(__name__)


def load_data_file(filepath: str) -> None:
    from .models import FirewallRule, VirtualMachine, VirtualMachineTag
    from .serializers import DataImportSerializer

    data_file = Path(filepath)

    raw_data = json.loads(data_file.read_bytes())
    serializer = DataImportSerializer(data=raw_data)

    _logger = logger.bind(path=str(data_file.absolute()))

    if serializer.is_valid():
        virtual_machines, virtual_machines_tags, firewall_rules = [], [], []

        for vm_data in serializer.validated_data["vms"]:
            virtual_machines.append(
                VirtualMachine(
                    vm_id=vm_data["vm_id"],
                    name=vm_data["name"],
                )
            )

            for tag in vm_data["tags"]:
                virtual_machines_tags.append(
                    VirtualMachineTag(vm_id=vm_data["vm_id"], name=tag)
                )

        for fw_rule_data in serializer.validated_data["fw_rules"]:
            firewall_rules.append(
                FirewallRule(
                    fw_id=fw_rule_data["fw_id"],
                    source_tag=fw_rule_data["source_tag"],
                    dest_tag=fw_rule_data["dest_tag"],
                )
            )

        with transaction.atomic():
            VirtualMachine.objects.all().delete()
            FirewallRule.objects.all().delete()

            VirtualMachine.objects.bulk_create(
                virtual_machines,
                ignore_conflicts=True,
            )
            VirtualMachineTag.objects.bulk_create(
                virtual_machines_tags,
                ignore_conflicts=True,
            )
            _logger.debug(
                "data_import.virtual_machines.imported",
                count=len(virtual_machines),
            )

            FirewallRule.objects.bulk_create(
                firewall_rules,
                ignore_conflicts=True,
            )
            _logger.debug(
                "data_import.firewall_rules.imported",
                count=len(firewall_rules),
            )
    else:
        _logger.warning(
            "data_import.failed",
            errors=serializer.errors,
        )
