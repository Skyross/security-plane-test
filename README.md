# Security Plane Test

This project is a test task for job application.

## Prepare

Create `.env` from the base file:

    cp .env-base .env

Install dependencies. NOTE: this project uses `poetry` to manage dependencies, so `poetry` will be installed to
your `$HOME/.poetry/bin/poetry`:

    make install

Set all settings at `.env`.

## Start up

    FIREWALLS_IMPORT_FILE=data/input-0.json make service

## Contributing

### Code formatting

    make fmt

This is an alias for:

    make fmt-black fmt-isort

### Code linting

    make lint

This is an alias for:

    make lint-bandit lint-black lint-flake8 lint-isort lint-mypy

### Testing

    make test

Open coverage report:

    open htmlcov/index.html
